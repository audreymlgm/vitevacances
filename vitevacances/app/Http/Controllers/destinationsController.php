<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pays;

class destinationsController extends Controller
{
    public function index()
    {
        $pays = pays::all();
        return view ('destinations', ['pays' => $pays]);
    }


    public function show($id)
    {
        $pays =  pays::find($id);
        return view('pays', ['pays' => $pays]);
    }
}
