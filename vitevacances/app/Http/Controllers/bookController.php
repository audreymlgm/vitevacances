<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\aeroports;
use App\pays;

class bookController extends Controller
{
    public function index()
    {
        $aeroports=aeroports::all();
        $pays = pays::all();
        return view ('book', ['aeroports' => $aeroports, 'pays' => $pays]);
    }
}
