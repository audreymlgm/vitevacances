<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aeroports extends Model
{
    protected $table = 'aeroports';
    protected $guarded = ['id'];
    protected $fillable= ['nom'];
}
