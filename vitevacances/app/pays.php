<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pays extends Model
{

        protected $table = 'pays';
        protected $guarded = ['id'];
        protected $fillable= ['nom', 'aeroports_id', 'background', 'picto1', 'picto2', 'picto3', 'texte'];

}
