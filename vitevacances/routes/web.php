<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'homeController@index')->name('home');
Route::get('home', 'homeController@index')->name('home');

Route::get('/destinations/', 'destinationsController@index')->name('destinations');

Route::get('/book/', 'bookController@index')->name('aeroports', 'pays');

Route::get('/pays/{id}', 'destinationsController@show')->name('pays');


Route::get('/getairport', 'getairportController@index')->name('getairport');

Route::get('/contact', 'contactController@index')->name('contact');

Route::get('/mentions', 'mentionsController@index')->name('mentions');

Route::get('/cgv', 'cgvController@index')->name('cgv');

Route::get('/politique', 'politiqueController@index')->name('politique');

Route::get('/connexion', 'connexionController@index')->name('connexion');




?>
