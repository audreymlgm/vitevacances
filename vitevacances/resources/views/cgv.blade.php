
@extends('layouts.app')

@section('content')

<section id="cgv">
    <h1> Nos Conditions Générales de Vente </h1>

    <p>     Les conditions générales de vente entre professionnels (ou B2B) doivent obligatoirement mentionner : les conditions de vente ; le barème des prix unitaires ; les éventuelles réductions de prix et conditions d'escompte ; les conditions de règlement, notamment les délais de paiement et pénalités de retard. Il est possible que les conditions générales de vente soient différenciées selon les catégories d'acheteurs (grossiste/détaillant par exemple). Lorsque le prix d'un service ou d'un type de service ne peut pas être déterminé a priori ou indiqué avec exactitude, le prestataire de services doit communiquer au destinataire qui en fait la demande la méthode de calcul du prix permettant de vérifier ce dernier, ou un devis suffisamment détaillé. Escompte L'escompte est une réduction consentie à un acheteur en cas de paiement anticipé. Le vendeur peut consentir un escompte pour paiement comptant qui, dès lors qu'il est proposé à tous les acheteurs, doit être mentionné dans les conditions de règlement. Cette réduction doit apparaître sur les factures. Réductions et rabais Les diminutions de prix doivent être fixées selon des critères précis et objectifs et peuvent avoir un caractère quantitatif ou qualitatif. Le vendeur doit aussi faire figurer les remises promotionnelles ponctuelles et les ristournes différées de fin d'année. Mentions facultatives Les CGV peuvent aussi prévoir des mentions facultatives, mais importantes, portant sur les conditions de résiliation du contrat, sur l'existence d'une clause de réserve de propriété, par exemple.
    </p>
</section>

@endsection()
