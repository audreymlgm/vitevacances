@extends('layouts.app')

@section('content')

<head>

</head>

<body>
 <!-- Slideshow container -->
 <section id="slide">
 <div class="slideshow-container">

<!-- Full-width 6 with number and caption text -->
<div class="mySlides fade">
  <div class="numbertext"></div>
  <img src="../media/Italie.jpg" width="1283px" height="550px">
  <div class="text">Séjour de <span class="bold">5 nuits en Italie</span> <br />
                    A partir de 499 euros <br />
                    <span class="bold">Hotel 5* </span><br />
                    Vol A/R <br />
  </div>
</div>

<div class="mySlides fade">
  <div class="numbertext"></div>
  <img src="../media/Croatie.jpg" width=1283px height="550px" >
  <div class="text">Séjour de <span class="bold">4 nuits en Croatie</span> <br />
                    A partir de 399 euros<br />
                    <span class="bold">Hotel 5* </span><br />
                    Vol A/R <br />
</div>
</div>

<div class="mySlides fade">
  <div class="numbertext"></div>
  <img src="../media/Malte.jpg" width="1283px" height="550px">
  <div class="text">Séjour de <span class="bold">4 nuits à Malte </span><br />
    A partir de 399 euros<br />
    <span class="bold">Hotel 5* </span><br />
    Vol A/R <br />
</div>
</div>

<div class="mySlides fade">
  <div class="numbertext"></div>
  <img src="../media/Portugal.jpg" width="1283px" height="550px">
  <div class="text">Séjour de <span class="bold">4 nuits au Portugal</span><br />
    A partir de 399 euros<br />
    <span class="bold">Hotel 5* </span><br />
    Vol A/R <br />
      </div>
</div>

<!-- Next and previous buttons -->
<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>

<!-- The dots/circles -->
<div style="text-align:center">
<span class="dot" onclick="currentSlide(1)"></span>
<span class="dot" onclick="currentSlide(2)"></span>
<span class="dot" onclick="currentSlide(3)"></span>
<span class="dot" onclick="currentSlide(4)"></span>
</div>
</section>

<section id="concept">
  <h1> Notre Concept </h1>
    <p> VOYAGEZ A TRAVERS <br /> <span class="bold">27 PAYS D’EUROPE <br /> </span> A <span class="bold">MOINDRE PRIX <br /> </span> GRACE A NOS <span class="bold">COURTS SEJOURS * <br /> </span><span class="italic"> *INFERIEURS à 7 JOURS </span></p>
</section>


  <h2> Les Bons Plans du Moment </h2>

  <section id="deal">
      <div class="images1">
        <a href="destinations">
            <div class="textes">
            <h2>Budapest</h2>
                <p>Aller/Retour</p>
                <p>A partir de 400€</p>
                <p><span class="bold"> Hôtel 4*</span></p>
                <p>7jours/6nuits</p>
            </div>
        </a>
      </div>
      <div class="images2">
        <a href="destinations">
            <div class="textes">
            <h2>Budapest</h2>
                <p>Aller/Retour</p>
                <p>A partir de 400€</p>
                <p><span class="bold"> Hôtel 4*</span></p>
                <p>7jours/6nuits</p>
            </div>
        </a>
    </div>
    <div class="images3">
        <a href="destinations">
            <div class="textes">
            <h2>Budapest</h2>
                <p>Aller/Retour</p>
                <p>A partir de 400€</p>
                <p><span class="bold"> Hôtel 4*</span></p>
                <p>7jours/6nuits</p>
            </div>
        </a>
    </div>
</section>
<section id="deal1">
    <div class="images4">
        <a href="destinations">
            <div class="textes">
            <h2>Budapest</h2>
                <p>Aller/Retour</p>
                <p>A partir de 400€</p>
                <p><span class="bold"> Hôtel 4*</span></p>
                <p>7jours/6nuits</p>
            </div>
        </a>
    </div>
    <div class="images5">
        <a href="destinations">
            <div class="textes">
                <h2>Budapest</h2>
                <p>Aller/Retour</p>
                <p>A partir de 400€</p>
                <p><span class="bold"> Hôtel 4*</span></p>
                <p>7jours/6nuits</p>
            </div>
        </a>
    </div>
    <div class="images6">
        <a href="destinations">
            <div class="textes">
                <h2>Budapest</h2>
                <p>Aller/Retour</p>
                <p>A partir de 400€</p>
                <p><span class="bold"> Hôtel 4*</span></p>
                <p>7jours/6nuits</p>
            </div>
        </a>
    </div>
</section>

<a href="destinations"><h2>Découvez le reste de nos destinations</h2></a>


<section id="timer">

  <div class="timer">
      <h2><span class="bold">DERNIERE JOURNEE</span> pour bénéficier de notre remise exceptionnelle de 20% avec le code promo* : </h2>
      <h3>VITEVACANCE20</h3>
      <p><span class="italic"> * Code Promo valable uniquement pour une réservation d'un montant minimum de 499euros </span></p>
    <ul>
      <li><span id="days"></span>Jours</li>
      <li><span id="hours"></span>Heures</li>
      <li><span id="minutes"></span>Minutes</li>
      <li><span id="seconds"></span>Secondes</li>
    </ul>
  </div>

</section>
<h2>Pourquoi choisir ViteVacances</h2>
<section id="garanty">
   <div class="garantyimg1">
      <img src="../media/thumb.png" alt="ViteVacance">
      <p> Numéro 1 Mondial du Voyage </p>
   </div>
   <div class="garantyimg2">
      <img src="../media/fly.png" alt="ViteVacance">
      <p> 27 destinations à moindre prix  </p>
   </div>

   <div class="garantyimg3">
      <img src="../media/hotel.png" alt="ViteVacance">
      <p> Des hôtels choisis avec soin </p>
   </div>

   <div class="garantyimg4">
      <img src="../media/payment.png" alt="ViteVacance">
      <p> Paiement Sécurisé </p>
   </div>
</section>


<script src="{{asset('js/script.js')}}" type="text/javascript"></script>

</body>
@endsection()
