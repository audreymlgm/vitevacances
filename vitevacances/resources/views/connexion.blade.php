
@extends('layouts.app')

@section('content')

<section id="connexion">

<form>
        <legend class="section_title">Connexion</legend>
        <label for="email">Mail</label>
        <input id="email" type="email" name="email" required>
        <label for="password">Mot de Passe</label>
        <input id="password" type="password" name="password" required>
        <input type="submit" value="Valider">
    </form>

</section>

@endsection()
