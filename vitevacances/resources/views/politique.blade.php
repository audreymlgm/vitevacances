
@extends('layouts.app')

@section('content')

<section id="politique">
    <h1> Notre Politique de Confidentialité </h1>
    <p>     La création et le traitement de données personnelles (numéro d'identifiant, nom, adresse, numéro de téléphone, photo, adresse IP notamment) sont soumis à des obligations destinées à protéger la vie privée et les libertés individuelles. De nouvelles obligations sont à la charge des entreprises, administrations, collectivités, associations ou autres organismes permettant d'accorder des droits plus étendus à leurs clients / usagers. Le régime des sanctions évolue également. Le règlement s'applique à tous les traitements de données à caractère personnel, sauf exceptions (les fichiers de sécurité restent régis par les États et les traitements en matière pénale par exemple).

            Il concerne :

                les responsables de traitement (entreprises, administrations, associations ou autres organismes) et leurs sous-traitants (hébergeurs, intégrateurs de logiciels, agences de communication entre autres) établis dans l'Union européenne (UE), quel que soit le lieu de traitement des données.

                les responsables de traitement et leurs sous-traitants établis hors de l'UE, quand ils mettent en œuvre des traitements visant à fournir des biens ou des services à des résidents européens ou lorsqu'ils les ciblent avec des techniques algorithmiques (technique du profilage).

            En pratique, le règlement s'applique donc à chaque fois qu'un résident européen, quelle que soit sa nationalité, est directement visé par un traitement de données, y compris par internet ou par le biais d'objets connectés (appareils domotiques, objets mesurant l'activité physique par exemple).
    </p>
</section>

@endsection()

