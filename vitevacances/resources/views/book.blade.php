@extends('layouts.app')

@section('content')

<section id="booking">

    <h1>Vous voulez réserver ? C'est par ici</h1>

                    <form method="POST" action="">

                    <label for="airport">Aéroports</label>
                    <select name="airport" size="1">
                    @foreach ( $aeroports as $aeroport )
                    <option> {{ $aeroport->nom }} </option>
                    @endforeach
                    </select>


                    <label for="pays">Destinations</label>
                    <select name="pays" size="1">
                    @foreach ( $pays as $pay )
                    <option> {{ $pay->nom }} </option>
                    @endforeach
                    </select>


                <label for="start">Date de départ</label>
                <input type="date" id="start" name="trip-start"
                value="2019-04-14"
                min="2019-04-11" max="2019-12-12">



                <label for="start">Date de retour</label>
                <input type="date" id="start" name="trip-start"
                value="2019-04-14"
                min="2019-04-11" max="2019-12-12">


                <label for="adult">Nombre d'adultes (0-20):</label>
                <input type="number" id="adult" name="adult"
                min="0" max="20">

                <label for="child">Nombre d'enfants (0-30):</label>
                <input type="number" id="child" name="child"
                min="0" max="20">

                <input type="submit" name="sendForm" value="Trouver une réservation">

            </form>


</section>


<script src="{{asset('js/script.js')}}" type="text/javascript"></script>


    @endsection()

