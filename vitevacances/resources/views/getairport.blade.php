@extends('layouts.app')

@section('content')

<section id="getairport">
    <h1>Comment se Rendre à l'aéroport ?<h1>
        <p>Nous travaillons avec 4 aéroports</p>
        <ul>
            <li>Aéroport Lesquin (Lille)</li>
            <li>Aéroport Charles de Gaulles (Paris)</li>
            <li>Aéroport Lyon-Saint Exupéry(Lyon)</li>
            <li>Aéroport de Bruxelles(Bruxelles)</li>
        </ul>
        <p> Pour savoir ou se situe l'aéroport en fonction de votre domicile, nous vous invitons à cocher l'aéroport désiré via le formulaire ci-dessous</p>


                    <form method="GET" action="">
                    <legend class="section_title">Distance Aéroport/Domicile</legend>
                    <label for="cdg">Aéroport Charles de Gaulle</label>
                    <input type="checkbox" id="cdg" name="cdg" @if(isset($_GET['cdg']) == 'on') checked @else @endif>
                    <label for="lille">Aéroport de Lesquin</label>
                    <input type="checkbox" id="lille" name="lille" @if(isset($_GET['lille']) == 'on') checked @else @endif>
                    <label for="charleroi">Aéroport Bruxelles-Charleroi</label>
                    <input type="checkbox" id="charleroi" name="charleroi" @if(isset($_GET['charleroi']) == 'on') checked @else @endif>
                    <label for="lyon">Aéroport Lyon-Saint Exupéry</label>
                    <input type="checkbox" id="lyon" name="lyon" @if(isset($_GET['lyon']) == 'on') checked @else @endif>
                    <input type="submit" name="sendForm" value="Afficher">
                </form>
</section>


@if(isset($_GET['lille']) == 'on')

   <center><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2533.9776326058295!2d3.1038983153071933!3d50.5717799794923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d68e7cc038d7%3A0x962e81e668425d14!2sA%C3%A9roport+de+Lille-Lesquin!5e0!3m2!1sfr!2sfr!4v1555017302398!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></center>
@else

@endif

@if(isset($_GET['cdg']) == 'on')

    <center><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2617.047011588238!2d2.545735815237086!3d49.00969057930256!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e63e038e4ccf5b%3A0x42be0982f5ba62c!2sParis-Charles+De+Gaulle!5e0!3m2!1sfr!2sfr!4v1555019027467!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></center>
@else

@endif

@if(isset($_GET['charleroi']) == 'on')

    <center><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2539.8489517316393!2d4.456632615185856!3d50.462537379477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c228d0db96d9e5%3A0xc4a2ed66e37660d2!2sBrussels+South+Charleroi+Airport!5e0!3m2!1sfr!2sfr!4v1555058819845!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></center>
@else

@endif

@if(isset($_GET['lyon']) == 'on')

    <center><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11142.143495904505!2d5.077322480163066!3d45.720347115674365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f4c92b009f84cd%3A0x86dc510cc9b255f0!2sA%C3%A9roport+Lyon-Saint+Exup%C3%A9ry!5e0!3m2!1sfr!2sfr!4v1555058786117!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></center>
@else


@endif
@endsection()
