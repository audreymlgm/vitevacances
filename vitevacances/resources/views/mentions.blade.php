
@extends('layouts.app')

@section('content')

<section id="mentions">


    <h1> Nos Mentions Légales </h1>
    <p>     Tous les sites internet édités à titre professionnel, qu'ils proposent des ventes en ligne ou non, doivent obligatoirement indiquer les mentions légales suivantes :

        pour un entrepreneur individuel : nom, prénom, domicile ;
        pour une société : raison sociale, forme juridique, adresse de l'établissement ou du siège social (et non pas une simple boîte postale), montant du capital social ;
        adresse de courrier électronique et numéro de téléphone ;
        pour une activité commerciale : numéro d'inscription au registre du commerce et des sociétés (RCS) ; numéro individuel d'identification fiscale numéro de TVA intracommunautaire ;
        pour une activité artisanale : numéro d'immatriculation au répertoire des métiers (RM) ;
        pour une profession réglementée : référence aux règles professionnelles applicables et au titre professionnel ;
        nom et adresse de l'autorité ayant délivré l'autorisation d'exercer quand celle-ci est nécessaire ;
        nom du directeur de la publication et coordonnées de l'hébergeur du site (nom, dénomination ou raison sociale, adresse et numéro de téléphone) ;
        pour un site marchand, conditions générales de vente (CGV) : prix (exprimé en euros et TTC), frais et date de livraison, modalités de paiement, service après-vente, droit de rétractation, durée de l'offre, coût de la technique de communication à distance ;

    Avant de déposer ou lire un cookie, les éditeurs de sites ou d'applications doivent :

        informer les internautes de la finalité des cookies ;
        obtenir leur consentement ;
        fournir aux internautes un moyen de les refuser.

    La durée de validité de ce consentement est de 13 mois maximum. Certains cookies sont cependant dispensés du recueil de ce consentement.

    Le manquement à l'une de ces obligations peut être sanctionné jusqu'à 1 an d'emprisonnement, 75 000 € d'amende pour les personnes physiques et 375 000 € d'amende pour les personnes morales.
    </p>


</section>

@endsection()
