<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->unsignedInteger('aeroports_id');
            $table->string('background');
            $table->string('picto1');
            $table->string('picto2');
            $table->string('picto3');
            $table->string('texte');
            $table->timestamps();
        });


        Schema::table('pays', function (Blueprint $table) {
            $table->foreign('aeroports_id')->references('id')->on('aeroports');
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pays');
    }
}
